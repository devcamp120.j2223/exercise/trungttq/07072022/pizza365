import 'bootstrap/dist/css/bootstrap.min.css'
import { Carousel } from 'react-bootstrap';

import pizza1 from '../asset/images/1.jpg';
import pizza2 from '../asset/images/2.jpg';
import pizza3 from '../asset/images/3.jpg';
import pizza4 from '../asset/images/4.jpg';



const CarouselSlide = () =>{

    return(
        <Carousel>
                <Carousel.Item>
                  <img
                    className="d-block w-100 img-radius"
                    src={pizza1}
                    alt="First slide"
                  />
                </Carousel.Item>

                <Carousel.Item>
                  <img
                    className="d-block w-100 img-radius"
                    src={pizza2}
                    alt="Second slide"
                  />
                </Carousel.Item>

                <Carousel.Item>
                  <img
                    className="d-block w-100 img-radius"
                    src={pizza3}
                    alt="Third slide"
                  />
                </Carousel.Item>

                <Carousel.Item>
                  <img
                    className="d-block w-100 img-radius"
                    src={pizza4}
                    alt="Forth slide"
                  />
                </Carousel.Item>
              </Carousel>
    )
}

export default CarouselSlide;