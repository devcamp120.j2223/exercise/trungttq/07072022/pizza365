import { Typography , Button, Grid} from "@mui/material";
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import PinterestIcon from '@mui/icons-material/Pinterest';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

const Footer = () =>{
    return(
        <div style={{backgroundColor:"orange", padding: "30px"}}>
            <Typography variant="h4" align="center">
                <b>pizza365 !!</b>
            </Typography>
            <Grid container justifyContent="center" marginTop={2}>
                <Button variant="contained" color="secondary">To the top</Button>
            </Grid>
            <Grid container justifyContent="center" marginTop={2}>
                <FacebookIcon/><TwitterIcon/><InstagramIcon/><PinterestIcon/><LinkedInIcon/>
            </Grid>
            <Grid container justifyContent="center" marginTop={2}>
            <Typography variant="body1" align="center" >
                <b>Power by Devcamp120</b>
            </Typography>
            </Grid>
        </div>
    )
}


export default Footer;

