import { Typography, Card, CardActionArea,CardMedia, CardContent, CardActions, Button , Grid} from "@mui/material";

import pizzaBacon from './imgPizza/bacon.jpg';
import pizzaHawaiian from './imgPizza/hawaiian.jpg';
import pizzaSeafood from './imgPizza/seafood.jpg';

import { useEffect, useState } from 'react'

const PizzaType = () =>{
    const [statusBtnSeafood, setStatusBtnSeafood] = useState("warning");
    const [statusBtnHawaiian, setStatusBtnHawaiian] = useState("warning");
    const [statusBtnBacon, setStatusBtnBacon] = useState("warning");

    const [pizzaType, setPizzaType] = useState("");

    const onBtnPizzaSeafood = () =>{
        setStatusBtnSeafood("success");
        setStatusBtnHawaiian("warning");
        setStatusBtnBacon("warning");
        setPizzaType("Seafood");
        console.log(pizzaType);
    }

    const onBtnPizzaHawaiian = () =>{
        setStatusBtnSeafood("warning");
        setStatusBtnHawaiian("success");
        setStatusBtnBacon("warning");
        setPizzaType("Hawaiian");
        console.log(pizzaType);
    }

    const onBtnPizzaBacon = () =>{
        setStatusBtnSeafood("warning");
        setStatusBtnHawaiian("warning");
        setStatusBtnBacon("success");
        setPizzaType("Bacon");
        console.log(pizzaType);
    }
    return(
        <>
            <Typography variant="h5" align="center" color='warning.main' marginTop={5}>
                <b>Chọn loại pizza bạn yêu thích.  </b>
            </Typography>
            <Grid container justifyContent={"space-between"} marginTop={2}>
                <Grid style={{width: "33%"}}>
                <Card>
                    <CardActionArea>
                    <CardMedia
                        component="img"
                        height="fit-content"
                        image={pizzaSeafood}
                        alt="green iguana"
                        />
                        <CardContent>
                        <Typography padding={2} variant="h5" style={{height: "200px"}}>
                            <b>OCEAN MANIA</b>

                            <Typography variant="body1" style={{color: "#999"}}>
                            PIZZA HẢI SẢN XỐT MAYONNAISE
                            </Typography>
                            <Typography variant="body1">
                            Xốt cà chua, phô mai Mozzarella, tôm mực, thanh cua và hành tây
                            </Typography>
                        </Typography>

                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button onClick={onBtnPizzaSeafood}  fullWidth size="small" color={statusBtnSeafood} variant="contained">
                        CHỌN
                        </Button>
                    </CardActions>
                </Card>
                </Grid>

                <Grid style={{width: "33%"}}>
                <Card>
                    <CardActionArea>
                    <CardMedia
                        component="img"
                        height="fit-content"
                        image={pizzaHawaiian}
                        alt="green iguana"
                        />
                        <CardContent>
                        <Typography padding={2} variant="h5" style={{height: "200px"}}>
                            <b>HAWAIIAN</b>

                            <Typography variant="body1" style={{color: "#999"}}>
                            PIZZA JAMBON DỨA KIỂU HAWAII
                            </Typography>
                            <Typography variant="body1">
                            Xốt cà chua, phô mai Mozzarella, thịt jambon và thơm
                            </Typography>
                        </Typography>

                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button onClick={onBtnPizzaHawaiian}  fullWidth size="small" color={statusBtnHawaiian} variant="contained">
                        CHỌN
                        </Button>
                    </CardActions>
                </Card>
                </Grid>
                <Grid style={{width: "33%"}}>
                <Card>
                    <CardActionArea>
                    <CardMedia
                        component="img"
                        height="fit-content"
                        image={pizzaBacon}
                        alt="green iguana"
                        />
                        <CardContent>
                        <Typography padding={2} variant="h5" style={{height: "200px"}}>
                            <b>CHESSY CHICKEN BACON</b>

                            <Typography variant="body1" style={{color: "#999"}}>
                            PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI
                            </Typography>
                            <Typography variant="body1">
                            Xốt phô mai, thịt gà, thịt heo muối, phô mai Mozzarella và cà chua
                            </Typography>
                        </Typography>

                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button onClick={onBtnPizzaBacon}  fullWidth size="small" color={statusBtnBacon} variant="contained">
                        CHỌN
                        </Button>
                    </CardActions>
                </Card>
                </Grid>
            </Grid>
        </>
    )
}

export default PizzaType;