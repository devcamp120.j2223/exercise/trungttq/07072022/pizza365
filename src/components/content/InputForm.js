import { Typography, Grid, Input, Button, Container ,Modal,Box} from "@mui/material";
import { useState } from "react";
import { Label } from "reactstrap";

const InputForm = () =>{
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [voucher, setVoucher] = useState("");
    const [message, setMessage] = useState("");

    const [alert, setAlert] = useState(false);

    const [status, setStatus] = useState("");

    const [headingMess, setHeadingMess] = useState("");
    const [contentModal, setContentModal] = useState("");

    const handleClose = () =>{
        setAlert(false)
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
      };

const onBtnSubmit = () =>{
    var vObjectRequest = {
        name: name,
        email:email,
        phone:phone,
        address: address,
        voucher: voucher,
        message: message
    }
    var isValiDateData = isValidate(vObjectRequest);
    if(isValiDateData){
        console.log(vObjectRequest);
        setAlert(true);
        setStatus("success");
        setHeadingMess("Thành công");
        setContentModal("Mẫu đơn nhập hợp lệ !!");
    }
}

const isValidate = (paramObjectRequest) =>{
    var vMailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if(paramObjectRequest.name === ""){
        setAlert(true);
        setStatus("error");
        setHeadingMess("Thất bại");
        setContentModal("Vui lòng nhập Họ tên");
        return false;
    }
    if(!paramObjectRequest.email.match(vMailformat)){
        setAlert(true);
        setStatus("error");
        setHeadingMess("Thất bại");
        setContentModal("Email không hợp lệ");
        return false;
    }
    if(paramObjectRequest.phone === ""){
        setAlert(true);
        setStatus("error");
        setHeadingMess("Thất bại");
        setContentModal("Vui lòng nhập phone");
        return false;
    }
    if(isNaN(parseInt(paramObjectRequest.phone, 10))){
        setAlert(true);
        setStatus("error");
        setHeadingMess("Thất bại");
        setContentModal("Số phone không hợp lệ");
        return false;
    }
    if(paramObjectRequest.address === ""){
        setAlert(true);
        setStatus("error");
        setHeadingMess("Thất bại");
        setContentModal("Vui lòng nhập Address");
        return false;
    }
    return true;
}

    return(
        <div style={{marginBottom: "60px"}}>
        <Typography variant="h5" align="center" color='warning.main' marginTop={5}>
                <b>Gửi đơn hàng</b>
        </Typography>
        <Grid container marginTop={2}>
            <Grid item xs={1}>
                <Label><b>Họ tên:</b></Label>
            </Grid>
            <Grid item xs={11}>
                <Input value={name} onChange={(event) =>{setName(event.target.value)}} fullWidth/>
            </Grid>
        </Grid>

        <Grid container marginTop={2}>
            <Grid item xs={1}>
                <Label><b>Email:</b></Label>
            </Grid>
            <Grid item xs={11}>
                <Input value={email} onChange={(event) =>{setEmail(event.target.value)}} fullWidth/>
            </Grid>
        </Grid>

        <Grid container marginTop={2}>
            <Grid item xs={1}>
                <Label><b>Phone:</b></Label>
            </Grid>
            <Grid item xs={11}>
                <Input value={phone} onChange={(event) =>{setPhone(event.target.value)}} fullWidth/>
            </Grid>
        </Grid>

        <Grid container marginTop={2}>
            <Grid item xs={1}>
                <Label><b>Địa chỉ:</b></Label>
            </Grid>
            <Grid item xs={11}>
                <Input value={address} onChange={(event) =>{setAddress(event.target.value)}} fullWidth/>
            </Grid>
        </Grid>

        <Grid container marginTop={2}>
            <Grid item xs={1}>
                <Label><b>Voucher:</b></Label>
            </Grid>
            <Grid item xs={11}>
                <Input value={voucher} onChange={(event) =>{setVoucher(event.target.value)}} fullWidth/>
            </Grid>
        </Grid>

        <Grid container marginTop={2}>
            <Grid item xs={1}>
                <Label><b>Lời nhắn:</b></Label>
            </Grid>
            <Grid item xs={11}>
                <Input value={message} onChange={(event) =>{setMessage(event.target.value)}} fullWidth/>
            </Grid>
        </Grid>
        <Grid container marginTop={5}>
            <Button onClick={onBtnSubmit} fullWidth variant="contained" color="warning">Gửi đơn</Button>
        </Grid>


{/* MODAL NOTICE DATA */}
        <Modal
            open={alert}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h5" component="h2" color={` ${status}.main `}>
                {headingMess}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                {contentModal}
                </Typography>
            </Box>
            </Modal>

        </div>
    )
}

export default InputForm;