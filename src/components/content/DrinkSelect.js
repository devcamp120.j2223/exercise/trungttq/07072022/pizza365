import { MenuItem, Select , Typography} from "@mui/material";
import {useState, useEffect} from 'react';

const DrinkSelect = () =>{

    const [drinkList, setDrinkList] = useState([]);
    const [drink, setDrink] = useState("");

    const callDrink = async (paramUrl, paramObject = {}) =>{
        const response = await fetch(paramUrl);
        const data = await response.json();
        return data;
    }

    useEffect(() =>{
        callDrink("http://42.115.221.44:8080/devcamp-pizza365/drinks")
        .then((data) =>{
            console.log(data);
            setDrinkList(data);
        })
        .catch((error) =>{
            console.log(error.message);
        })
    }, [])

    return(
        <>
            <Typography variant="h5" align="center" color='warning.main' marginTop={5}>
                <b>Chọn loại nước uống mà bạn yêu thích.  </b>
            </Typography>
            <Select
                fullWidth
                defaultValue="ALL"
                marginTop={2}
                onChange={(event) =>{setDrink(event.target.value)
                                    console.log(drink)}
                                }
            >
                <MenuItem value="ALL">Chọn loại nước uống</MenuItem>
                {drinkList.map((element, index) =>{
                    return(
                        <MenuItem key={index} value={element.maNuocUong}>{element.tenNuocUong}</MenuItem>
                    )
                })}

            </Select>
        </>

    )
}


export default DrinkSelect;