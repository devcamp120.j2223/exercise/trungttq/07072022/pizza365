import DrinkSelect from "./content/DrinkSelect";
import Info from "./content/Info";
import InputForm from "./content/InputForm";
import PizzaSize from "./content/PizzaSize";
import PizzaType from "./content/PizzaType";


const Content = () =>{
    return(
        <>
            <Info/>
            <PizzaSize/>
            <PizzaType/>
            <DrinkSelect/>
            <InputForm/>
        </>
    )
}

export default Content;